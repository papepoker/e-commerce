-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 21, 2013 at 08:41 AM
-- Server version: 5.5.25
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `about_detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `about_detail`) VALUES
(2, 'ร้านดอกหญ้าการขาย จำหน่ายสินค้าทุกรูปแบบ');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `admin_username`, `admin_password`) VALUES
(1, 'admin', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `bill_sales`
--

CREATE TABLE `bill_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `bill_sale_name` varchar(255) DEFAULT NULL,
  `bill_sale_created_date` datetime NOT NULL,
  `bill_sale_status` enum('wait','pay','send') NOT NULL,
  `bill_sale_pay_date` datetime DEFAULT NULL,
  `bill_sale_send_date` datetime DEFAULT NULL,
  `bill_sale_tel` varchar(255) DEFAULT NULL,
  `bill_sale_email` varchar(255) DEFAULT NULL,
  `bill_sale_address` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bill_sales`
--

INSERT INTO `bill_sales` (`id`, `member_id`, `bill_sale_name`, `bill_sale_created_date`, `bill_sale_status`, `bill_sale_pay_date`, `bill_sale_send_date`, `bill_sale_tel`, `bill_sale_email`, `bill_sale_address`) VALUES
(2, NULL, 'ถาวร ศรีเสนพิลา', '2013-05-20 13:21:44', 'wait', NULL, NULL, '0868776053', 'karoe@mail.co', 'm test');

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `detail` text NOT NULL,
  `board_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`id`, `member_id`, `topic`, `created_date`, `detail`, `board_count`) VALUES
(1, 3, 'test', '2013-05-14 15:11:44', 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `board_comments`
--

CREATE TABLE `board_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `detail` text NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `board_comments`
--

INSERT INTO `board_comments` (`id`, `board_id`, `member_id`, `detail`, `created_date`) VALUES
(1, 1, 3, 'comment 1', '2013-05-14 16:08:49'),
(2, 1, 3, 'comment 2', '2013-05-14 16:13:43');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `contact_detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_detail`) VALUES
(1, '33 หมู่ 8 ต.หนองกระต่าย บ้านตาดโตน ตลาดหนองกระต่าย');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `u_username` varchar(255) NOT NULL,
  `u_password` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `sex` enum('male','female') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `u_username`, `u_password`, `tel`, `email`, `created_date`, `sex`) VALUES
(3, 'ตะวัน ย่องดินนะ', 'member1', '1234', '04432342342', 'tawan@mail.com', '2013-05-09 15:04:15', 'male');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `bill_sale_id` int(11) NOT NULL,
  `order_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `product_id`, `bill_sale_id`, `order_price`) VALUES
(32, 4, 2, 100),
(33, 5, 2, 300),
(34, 5, 2, 300);

-- --------------------------------------------------------

--
-- Table structure for table `pays`
--

CREATE TABLE `pays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_bank_name` varchar(255) NOT NULL,
  `pay_bank_code` varchar(50) NOT NULL,
  `pay_bank_type` varchar(255) NOT NULL,
  `pay_bank_branch` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pays`
--

INSERT INTO `pays` (`id`, `pay_bank_name`, `pay_bank_code`, `pay_bank_type`, `pay_bank_branch`) VALUES
(1, 'กสิกรไทย', '134993423874', 'ออมทรัพย์', 'กทม.');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `image` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `image`, `detail`) VALUES
(4, 'หนังสือ Yii Basic', 100, 'yii_basic.png', ''),
(5, 'หนังสือ Yii Advanced', 300, 'cover.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type_name` varchar(255) NOT NULL,
  `product_type_code` varchar(255) NOT NULL,
  `product_type_detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`id`, `product_type_name`, `product_type_code`, `product_type_detail`) VALUES
(1, 'กระเป๋า', '10011', 'กระเป๋าทุกรูปแบบ'),
(2, 'เสื้อผ้า', '10012', 'เสื้อผ้า และกระโปรง กางเกง');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_name` varchar(255) NOT NULL,
  `shipping_detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `shipping_name`, `shipping_detail`) VALUES
(1, 'ทางไปรษณีย์', 'ส่งถึงบ้าน'),
(2, 'ทางเรือ', 'มารับเองที่ท่าเรือครับ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
