class BackendController < ApplicationController
  def index
    if session[:admin] == nil
      redirect_to :action => "AdminLogin"
    end
  end

  #
  # ProductType
  #
  def ProductTypeIndex
    if params[:id] != nil
      @product_type = ProductType.find(params[:id])
    end

    @productTypes = ProductType.find(:all)
  end

  def ProductTypeSave
    if params[:product_type][:id] == ""
      productType = ProductType.new(params[:product_type])
    else
      id = params[:product_type][:id]
      productType = ProductType.find(id)
      productType.update_attributes(params[:product_type])
    end

    if productType.save
      redirect_to :action => "ProductTypeIndex"
    end
  end

  def ProductTypeDelete
    ProductType.delete(params[:id])
    redirect_to :action => "ProductTypeIndex"
  end

  #
  # AboutUs
  #
  def AboutUsIndex
    if params[:about_us] != nil
      if params[:about_us][:id] == ""
        about_us = AboutUs.new(params[:about_us])
      else
        about_us = AboutUs.find(:first)
        about_us.update_attributes(params[:about_us])
      end

      if about_us.save
        redirect_to :action => "AboutUsIndex"
      end
    end

    @about_us = AboutUs.find(:first)
  end

  #
  # Contact
  #
  def ContactIndex
    if params[:contact] != nil
      if params[:contact][:id] == ""
        contact = Contact.new(params[:contact])
      else
        contact = Contact.find(:first)
        contact.update_attributes(params[:contact])
      end

      if contact.save
        redirect_to :action => "ContactIndex"
      end
    end

    @contact = Contact.find(:first)
  end

  #
  # Shipping
  #
  def ShippingIndex
    @shippings = Shipping.find(:all)

    if params[:id] != nil
      @shipping = Shipping.find(params[:id])
    end
  end

  def ShippingSave
    if params[:shipping] != nil
      if params[:shipping][:id] == ""
        shipping = Shipping.new(params[:shipping])
      else
        shipping = Shipping.find(params[:shipping][:id])
        shipping.update_attributes(params[:shipping])
      end

      if shipping.save
        redirect_to :action => "ShippingIndex"
      end
    end
  end

  def ShippingDelete
    Shipping.delete(params[:id])
    redirect_to :action => "ShippingIndex"
  end

  #
  # Pay
  #
  def PayIndex
    if params[:pay] != nil
      if params[:pay][:id] == ""
        pay = Pay.new(params[:pay])
      else
        pay = Pay.find(params[:pay][:id])
        pay.update_attributes(params[:pay])
      end

      if pay.save
        redirect_to :action => "PayIndex"
      end
    end

    # edit
    if params[:id] != nil
      @pay = Pay.find(params[:id])
    end

    @pays = Pay.find(:all)
  end

  def PayDelete
    Pay.delete(params[:id])
    redirect_to :action => "PayIndex"
  end

  #
  # Admin
  #
  def AdminLogin
    if params[:admin] != nil
      u = params[:admin][:admin_username]
      p = params[:admin][:admin_password]

      admin = Admin.where(
        :admin_username => u, 
        :admin_password => p
      ).first

      if admin != nil
        session[:admin] = admin
        redirect_to :action => "index"
      else
        @message = "Username Invalid"
      end
    end
  end

  def AdminLogout
    session[:admin] = nil
    redirect_to :action => "AdminLogin"
  end

  def AdminChangeProfile
    id = session[:admin].id
    @admin = Admin.find(id)

    if params[:admin] != nil
      @admin.update_attributes(params[:admin])

      if params[:admin][:admin_password] == ""
        @admin.admin_password = session[:admin].admin_password
      end

      if @admin.save
        redirect_to :action => "AdminLogout"
      end
    end
  end

  #
  # Product
  #
  def ProductIndex
    @products = Product.find(:all)
  end

  def ProductForm
    if params[:id] != nil
      @product = Product.find(params[:id])
    end
  end

  def ProductSave
    if params[:product] != nil
        product = Product.new
        
        if params[:product][:id] != ""
            product = Product.find(params[:product][:id])
        else
            product.image = ""
        end
    
      # upload
      if params[:product][:image] != nil
        img = params[:product][:image]
        name = img.original_filename
        directory = "app/assets/images"

        path = File.join(directory, name)
        File.open(path, "wb") { |f| f.write(img.read) }
        
        product.image = name
      end

      # attributes
      product.name = params[:product][:name]
      product.price = params[:product][:price]
      product.detail = params[:product][:detail]
      
      # save
      if product.save
        redirect_to :action => "ProductIndex"
      end
    end
  end

  def ProductDelete
    Product.delete(params[:id])
    redirect_to :action => "ProductIndex"
  end
  
  #
  # Order
  #
  def OrderIndex
    @bill_sales = BillSale.find(
      :all,
      :order => "id DESC"
    )
  end
  
  def OrderPay
    bill = BillSale.find(params[:id])
    bill.bill_sale_status = "pay"
    bill.bill_sale_pay_date = Time.now
    bill.save
    
    redirect_to :action => "OrderIndex"
  end
  
  def OrderSend
    bill = BillSale.find(params[:id])
    bill.bill_sale_status = "send"
    bill.bill_sale_send_date = Time.now
    bill.save
    
    redirect_to :action => "OrderIndex"
  end
  
  def OrderDelete
    BillSale.delete(params[:id])
    orders = Order.where(:bill_sale_id => params[:id])

    orders.each do |r|
      r.delete
    end

    redirect_to :action => "OrderIndex"
  end
  
  #
  # Report
  #
  def ReportSale
    @n = 1
    @sum = 0
    @orders = Order.find(
      :all,
      :select => "
        orders.*, 
        bill_sales.bill_sale_created_date,
        bill_sales.bill_sale_pay_date,
        bill_sales.bill_sale_name",
      :joins => "
        LEFT JOIN bill_sales ON bill_sales.id = orders.bill_sale_id",
      :conditions => "
        DAY(bill_sale_pay_date) = DAY(NOW()) 
        AND MONTH(bill_sale_pay_date) = MONTH(NOW()) 
        AND YEAR(bill_sale_pay_date) = YEAR(NOW())
        AND bill_sale_status IN('pay', 'send')
      "
    )
  end
  
  def ReportSalePerDay
    if params[:month] != nil
      @sum = 0
      @month = params[:month]
      @year = params[:year]
      
      @orders = BillSale.find(
        :all,
        :select => "
          DAY(bill_sale_pay_date) AS day_sale,
          (
            SELECT SUM(order_price) FROM orders
            WHERE bill_sale_id = bill_sales.id
          ) AS sum_price
          ", 
        :conditions => "
          MONTH(bill_sale_pay_date) = #{@month}
          AND YEAR(bill_sale_pay_date) = #{@year}
          AND bill_sale_status IN ('pay', 'send')"
      )
    end
  end
  
  def ReportSalePerMonth
    if params[:year] != nil
      @sum = 0
      @year = params[:year]
      @orders = BillSale.find(
        :all,
        :select => "
          MONTH(bill_sale_pay_date) AS month_sale,
          (
            SELECT SUM(order_price) FROM orders
            WHERE orders.bill_sale_id = bill_sales.id
          ) AS sum_price
          ",
        :conditions => "
          YEAR(bill_sale_pay_date) = #{@year}
          AND bill_sale_status IN ('pay', 'send')"
      )
    end
  end

  def BillSaleView
    @bill_sale = BillSale.find(params[:id])
    @orders = Order.where(:bill_sale_id => params[:id])
  end
end
