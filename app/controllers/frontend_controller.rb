class FrontendController < ApplicationController
  
  layout "frontend_layout"  
  
  def index
    @products = Product.find(:all)
    @count_item = 0;
  end

  def AddToCart
    if session[:cart] == nil
      cart = Array.new
      cart[0] = params[:id]

      session[:cart] = cart
    else
      cart = session[:cart]
      cart[cart.count] = params[:id]
      session[:cart] = cart
    end

    redirect_to :action => "index"
  end

  def CheckOut
    if params[:order] != nil
      # validate
      valid = true

      if params[:order][:order_name] == ""
        flash[:message] = "name is not empty"
        valid = false        
      end

      if params[:order][:order_tel] == ""
        flash[:message] = "tel is not empty"
        valid = false
      end

      if params[:order][:order_email] == ""
        flash[:message] = "email is not empty"
        valid = false
      end

      if params[:order][:order_address] == ""
        flash[:message] = "address is not empty"
        valid = false
      end

      if !valid
        return
      end

      # add to bill_sale
      billSale = BillSale.new
      billSale.bill_sale_created_date = Time.now
      billSale.bill_sale_status = 'wait'
      billSale.bill_sale_name = params[:order][:order_name]
      billSale.bill_sale_tel = params[:order][:order_tel]
      billSale.bill_sale_email = params[:order][:order_email]
      billSale.bill_sale_address = params[:order][:order_address]

      if session[:member]
        billSale.member_id = session[:member].id
      end

      # add to order
      if billSale.save
        session[:cart].each do |item|
          product = Product.find(item)
          order = Order.new
          order.bill_sale_id = billSale.id
          order.product_id = product.id
          order.order_price = product.price
          order.save
        end
      end

      session[:cart] = nil
      redirect_to :action => "CheckOutComplete"
    end
  end

  def CheckOutComplete
  end

  def RemoveFromCart
    index = params[:index]
    cart = session[:cart]
    cart.delete_at(index.to_f)
    
    session[:cart] = cart
    redirect_to :action => "index"
  end
  
  def about
    @about = AboutUs.find(:first)
  end
  
  def pay
    @pays = Pay.find(:all)
  end
  
  def shipping
    @shippings = Shipping.find(:all)
  end
  
  def MemberRegister
    if params[:member] != nil
      member = Member.new(params[:member])
      member.created_date = Time.now
      
      if member.save
        redirect_to :action => "MemberRegisterComplete"
      end
    end
  end
  
  def MemberRegisterComplete
  end
  
  def MemberLogin
    if params[:u_username] != nil or params[:u_password] != nil
      member = Member.where(
          :u_username => params[:u_username],
          :u_password => params[:u_password]
      ).first
      
      if member
        session[:member] = member
      else
        flash[:message] = "Username Invalid"        
      end
      
      redirect_to :action => "index"
    end
  end
  
  def MemberLogout
    session[:member] = nil
    redirect_to :action => "index"
  end
  
  def MemberChangeProfile
    @member = session[:member]
    
    if params[:member]
      @member.update_attributes(params[:member])
      
      if params[:member][:u_password] == ""
        @member.u_password = session[:member][:u_password]
      end
      
      if @member.save
        redirect_to :action => "index"
      end
    else
      render "MemberRegister"
    end
  end
  
  def History
    @bill_sales = BillSale.where(:member_id => session[:member].id)
  end
  
  def ProductTypeDetail
  end
  
  def BoardIndex
    @boards = Board.find(:all)
  end
  
  def BoardPost
    
  end
  
  def BoardSave
    if params[:board]
      board = Board.new(params[:board])
      board.board_count = 0
      board.created_date = Time.now
      board.member_id = session[:member].id

      if board.save
        redirect_to :action => "BoardIndex"
      end
    end
  end
  
  def BoardView
    @board = Board.find(params[:id])
    @board_comments = BoardComment.find(
      :all,
      :conditions => "board_id = #{params[:id]}",
      :order => "id DESC"
    )
  end
  
  def BoardComment
    if params[:board_comment]
      board_comment = BoardComment.new
      board_comment.member_id = session[:member].id
      board_comment.board_id = params[:board_id]
      board_comment.detail = params[:board_comment][:detail]
      board_comment.created_date = Time.now
      
      if board_comment.save
        redirect_to :action => "BoardView", :id => params[:board_id]
      end
    end
  end

  def VisitMe
  end

  def BillSaleDetail
    id = params[:id]
    @bill_sale = BillSale.find(id)
    @orders = Order.where(:bill_sale_id => id)
  end
    
end