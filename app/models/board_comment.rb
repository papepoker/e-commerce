class BoardComment < ActiveRecord::Base
	attr_accessible :member_id, :board_id, :detail, :created_date
	belongs_to :member
	belongs_to :board
end