class Order < ActiveRecord::Base
  attr_accessible :product_id, :bill_sale_id, :order_price
  belongs_to :product
  belongs_to :member
  belongs_to :bill_sale
end