class Board < ActiveRecord::Base
	attr_accessible :member_id, :topic, :created_date, :detail, :board_count
	belongs_to :member
end