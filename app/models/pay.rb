class Pay < ActiveRecord::Base
    attr_accessible :pay_bank_name, :pay_bank_code, :pay_bank_type, :pay_bank_branch
end
