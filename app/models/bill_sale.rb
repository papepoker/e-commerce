class BillSale < ActiveRecord::Base
	attr_accessible :member_id, :bill_sale_created_date, :bill_sale_status, :bill_sale_pay_date, :bill_sale_send_date
	belongs_to :member
end