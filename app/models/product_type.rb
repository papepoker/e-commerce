class ProductType < ActiveRecord::Base
  attr_accessible :product_type_code, :product_type_name, :product_type_detail
end
